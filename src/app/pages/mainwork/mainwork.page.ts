import { Component, OnInit } from '@angular/core';
import { NavController, AlertController } from '@ionic/angular';

@Component({
  selector: 'app-mainwork',
  templateUrl: './mainwork.page.html',
  styleUrls: ['./mainwork.page.scss'],
})
export class MainworkPage implements OnInit {

  constructor(
    private navCtrl: NavController, public alertController: AlertController
  ) { }

  ngOnInit() {
  }

  checklist(){
    this.navCtrl.navigateRoot("checklist");
  }

  logout(){
    this.navCtrl.navigateRoot("home");
  }

  changepassword(){
    this.navCtrl.navigateRoot("changepassword");
  }

  confirm(){
    this.navCtrl.navigateRoot("checklist");
  }
}
