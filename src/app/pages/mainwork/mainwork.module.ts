import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MainworkPageRoutingModule } from './mainwork-routing.module';

import { MainworkPage } from './mainwork.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MainworkPageRoutingModule
  ],
  declarations: [MainworkPage]
})
export class MainworkPageModule {}
