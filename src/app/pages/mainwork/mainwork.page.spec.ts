import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MainworkPage } from './mainwork.page';

describe('MainworkPage', () => {
  let component: MainworkPage;
  let fixture: ComponentFixture<MainworkPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MainworkPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(MainworkPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
