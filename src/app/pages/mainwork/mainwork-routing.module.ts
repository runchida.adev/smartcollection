import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MainworkPage } from './mainwork.page';

const routes: Routes = [
  {
    path: '',
    component: MainworkPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MainworkPageRoutingModule {}
