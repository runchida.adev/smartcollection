import { Component, OnInit } from '@angular/core';
import { NavController, AlertController } from '@ionic/angular';

@Component({
  selector: 'app-forgotpassword',
  templateUrl: './forgotpassword.page.html',
  styleUrls: ['./forgotpassword.page.scss'],
})
export class ForgotpasswordPage implements OnInit {

  constructor(
    private navCtrl: NavController, public alertController: AlertController
  ) { }

  ngOnInit() {
  }

  cancel(){
    this.navCtrl.navigateRoot("home");
  }

  confirm(){
    this.navCtrl.navigateRoot("home");
  }
}
