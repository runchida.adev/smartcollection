import { Component, OnInit } from '@angular/core';
import { NavController, AlertController } from '@ionic/angular';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.page.html',
  styleUrls: ['./welcome.page.scss'],
})
export class WelcomePage implements OnInit {

  constructor(
    private navCtrl: NavController, public alertController: AlertController
  ) { }

  ngOnInit() {
    setTimeout(() => {
      this.navCtrl.navigateRoot("home");
  }, 3000);
  }

}
