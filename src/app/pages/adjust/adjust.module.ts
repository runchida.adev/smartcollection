import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AdjustPageRoutingModule } from './adjust-routing.module';

import { AdjustPage } from './adjust.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AdjustPageRoutingModule
  ],
  declarations: [AdjustPage]
})
export class AdjustPageModule {}
