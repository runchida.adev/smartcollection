import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AdjustPage } from './adjust.page';

describe('AdjustPage', () => {
  let component: AdjustPage;
  let fixture: ComponentFixture<AdjustPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdjustPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AdjustPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
