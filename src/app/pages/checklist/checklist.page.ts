import { Component, OnInit } from '@angular/core';
import { NavController, AlertController } from '@ionic/angular';

@Component({
  selector: 'app-checklist',
  templateUrl: './checklist.page.html',
  styleUrls: ['./checklist.page.scss'],
})
export class ChecklistPage implements OnInit {

  constructor(
    private navCtrl: NavController, public alertController: AlertController
  ) { }

  ngOnInit() {
  }

  mainwork(){
    this.navCtrl.navigateRoot("mainwork");
  }

  logout(){
    this.navCtrl.navigateRoot("home");
  }

  changepassword(){
    this.navCtrl.navigateRoot("changepassword");
  }
}
