import { Component, OnInit } from '@angular/core';
import { NavController, AlertController } from '@ionic/angular';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {

  constructor(
    private navCtrl: NavController, public alertController: AlertController
  ) { }

  ngOnInit() {
  }

  login(){
    this.navCtrl.navigateRoot("login");
  }

  forgot(){
    this.navCtrl.navigateRoot("forgotpassword");
  }
}
