import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(public http: HttpClient) { }  

  login(username: string, password: string): Observable<any> {

    let URL = 'http://smartcollectiondev.iconsiam.com/api/login';
    let body = {
      "username": username,
      "password": password
    };

    console.log(body);

    return this.http.post<any>(URL, body);
  }
}
